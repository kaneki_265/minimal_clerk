
export interface ArticleItemType {
  title: string;
  author: string;
  url: string;
}

export const articleList:ArticleItemType[]  = [
  { 
    title: 'What I Do When I Feel Like Giving Up',
    author: 'James Clear',
    url: 'https://jamesclear.com/giving-up' 
  },
  { 
    title: 'The Proven Path to Doing Unique and Meaningful Work',
    author: 'James Clear',
    url: 'https://jamesclear.com/stay-on-the-bus' 
  },
  { 
    title: 'The Surprising Benefits of Journaling One Sentence Every Day',
    author: 'James Clear',
    url: 'https://jamesclear.com/journaling-one-sentence' 
  },
  { 
    title: 'How to Do Great Work',
    author: 'Paul Graham',
    url: 'https://paulgraham.com/greatwork.html' 
  },
  { 
    title: 'How to Get Startup Ideas',
    author: 'Paul Graham',
    url: 'https://paulgraham.com/startupideas.html' 
  },
  { 
    title: 'Crazy New Ideas',
    author: 'Paul Graham',
    url: 'https://paulgraham.com/newideas.html' 
  },

];

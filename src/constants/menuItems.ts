
import { AudioLines, Workflow, MessageCircle,Highlighter } from 'lucide-react';


export enum TabItemValue {
  TranslateListen = "TranslateListen",
  Chat = "Chat",
  Summary = "Summary",
  MindMap = "Mind Map",
}

export interface TabItemType {
  key: TabItemValue;
  title: string;
  href: string;
  icon: any;
}

export const menuItems: TabItemType[] = [
  {
    key: TabItemValue.TranslateListen,
    title: "Translate & Listen",
    href: "#",
    icon: AudioLines,
  },
  // {
  //   key: TabItemValue.Chat,
  //   title: "Chat",
  //   href: "#",
  //   icon: MessageCircle,
  // },
  // {
  //   key: TabItemValue.Summary,
  //   title: "Summary",
  //   href: "#",
  //   icon: Highlighter,
  // },
  // {
  //   key: TabItemValue.MindMap,
  //   title: "Mind Map",
  //   href: "#",
  //   icon: Workflow,
  // },
];



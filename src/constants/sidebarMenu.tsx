import { Home, NotebookText } from 'lucide-react';
import { URL_COLLECTION } from './url';


export const sidebarMenu = [
  {
      key:'home',
      label: 'Home',
      icon: <Home />,
      route: '/',
  },
  {
      key:'blog-collection',
      label: 'Blog Collection',
      icon: <NotebookText />,
      route: URL_COLLECTION,
  },
]

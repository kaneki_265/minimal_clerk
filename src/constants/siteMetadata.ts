import { Env } from "./env";

const siteMetadata = {
  title: 'Listening to Blog Articles and News in your Own Languages',
  author: 'Bisounds',
  headerTitle: 'Bisounds',
  // description: 'Listening to Blog Articles and News in your Own Languages',
  description: 'Listening to Blog Articles and News in your Own Languages - Read blog articles aloud - Translate blogs into any language - Bilingual learning',
  language: 'en-us',
  theme: 'system', // system, dark or light
  siteUrl: Env.siteUrl, 
  siteLogo: '/logo.svg',
  socialBanner: '/banner.png',
  email: Env.email,
  locale: 'en-US',
}

export default siteMetadata;
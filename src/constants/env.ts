export const Env = {
  email: `${process.env.NEXT_PUBLIC_EMAIL}`,
  emailPassword : process.env.EMAIL_PASSWORD || '',
  siteUrl: process.env.NEXT_PUBLIC_SITE_URL,
  gmailAppPassword: process.env.GMAIL_APP_PASS,
  trackingId: process.env.TRACKING_ID || ''
};

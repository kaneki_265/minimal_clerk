import { TabItemValue } from "@/constants/menuItems";

interface Props {
  icon?: any;
  title?: string;
  isActive?: boolean;
  onChange: (value: TabItemValue) => void;
  value: TabItemValue;

}

const TabItem = ({
  icon: Icon,
  title,
  onChange,
  value,
  isActive,
}: Props) => {
  return (
    <div
      className={`flex items-center px-3 py-2 text-sm font-medium ${
        isActive
          ? "text-white bg-gray-900" // Apply these styles when the tab is active
          : "text-gray-300 hover:text-white hover:bg-gray-700"
      } rounded-md cursor-pointer`}
      onClick={() => onChange(value)}
    >
      <Icon className="h-6 w-6 mr-1" aria-hidden="true" />
      {title}
    </div>
  );
};

export default TabItem;

import { menuItems, TabItemValue } from "@/constants/menuItems";
import MenuItem from "./TabItem";

interface TabBarProps {
  activeValue: string;
  onChange: (value: TabItemValue) => void;
}

const TabBar: React.FC<TabBarProps> = ({ activeValue, onChange }) => {
  return (
    <nav className="bg-gray-800 text-white shadow-lg w-full rounded">
      <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <div className="flex justify-between h-16">
          <div className="flex space-x-4">
            {menuItems.map((item) => (
              <MenuItem
                key={item.key}
                value={item.key}
                icon={item.icon}
                title={item.title}
                onChange={onChange}
                isActive={item.key === activeValue}
              />
            ))}
          </div>
        </div>
      </div>
    </nav>
  );
};

export default TabBar;

import { LoadingSpinner } from "./loading";

export const ComponentLoading = () => {

  return (
    <div className="inset-0  w-full h-full absolute flex items-center justify-center bg-white bg-opacity-10">
      <LoadingSpinner />
    </div>
  )
};



'use client'
import _get from "lodash/get";
import { useController } from "react-hook-form";
import BiFieldContainer from "./BiFieldContainer";



interface Props {
  name: string;
  label: string;
  control: any;
  type?: string;
  placeholder?: string;
  required?: boolean;
  md?: number;
  pr?: string;
  rows?: number;
  disabled?: boolean;
  background?: string;
  isTextArea?: boolean;
}

const BiInput = ({
  name,
  label,
  control,
  placeholder,
  md = 12,
  pr,
  isTextArea=false,
  disabled = false,
  type,
  rows=2,
  background
}: Props) => {
  const {
    field,
    formState: { errors }
  } = useController({
    name,
    control
  });

  const error = _get(errors, name, undefined);

  const clearInput = () => {
    field.onChange('')
  };


  return (
    <BiFieldContainer md={md} pr={pr}>
      <div className="flex flex-1 flex-col mb-2">
        <p className="text mb-4">
          {label}
        </p>
        <div className="relative">
          {
            !isTextArea ? (
              <input
                type="text"
                className="w-full p-2 border border-gray-300 rounded mr-2 pr-8"
                placeholder={placeholder}
                {...field}
              />
            ):
            (
              <textarea 
                rows={rows}
                className="w-full p-2 border border-gray-300 rounded mr-2 pr-8"
                placeholder={placeholder}
                {...field}
              />
            )
          }
          {true && (
            <button
              className="absolute top-0 right-0 mt-2 mr-3 text-gray-600 hover:text-gray-800"
              onClick={clearInput}
            >
              &#x2715;
            </button>
          )}
        </div>
      </div>
      <p className="text-orange-500">
        {error?.message?.toString()}
      </p>
    </BiFieldContainer>
  );
};

export default BiInput;
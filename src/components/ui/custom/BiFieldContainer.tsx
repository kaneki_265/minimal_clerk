
type Props =  {
  md: number;
  children: React.ReactNode;
  pr?: string;
};


const BiFieldContainer = (props: Props) => {
  const { md, children, pr } = props;

  return (
    <div 
      className={`flex flex-col pr-4 w-${md}/12`}
    >
      {children}
    </div>

  );
};

export default BiFieldContainer;

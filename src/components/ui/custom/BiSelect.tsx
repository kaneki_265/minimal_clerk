'use client'
import * as React from "react"
import _get from "lodash/get";

import {
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectLabel,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import BiFieldContainer from "./BiFieldContainer";
import { useController } from "react-hook-form";
import { FormLabel } from "../form";

type Option = {
  value: any,
  label: any
}

interface Props {
  name: string;
  label?: string;
  control?: any;
  placeholder?: string;
  options: Option[];
  isSearchable?: boolean;
  md?: number;
  disabled?: boolean;
  handleOnChange?: any;
  defaultValue?: any;
}

export function BiSelect(props: Props) {

  const {
    name,
    label,
    control,
    placeholder,
    options,
    md = 12,
    handleOnChange,
    disabled = false,
    isSearchable = false,
    defaultValue
  } = props;

  const {
    field: { onChange, value },
    formState: { errors },

  } = useController({
    name,
    control
  });

  const handleOnChangeInComponent = (value: any) => {
    if (typeof handleOnChange === "function") {
      handleOnChange(value);
    }

    onChange(value);
  };

  const error = _get(errors, name, undefined);

  return (
    <BiFieldContainer md={md}>
      <div className="flex flex-1 flex-col mb-2">
        <p className="text mb-4">
          {label}
        </p>
  
        <Select onValueChange={handleOnChangeInComponent} defaultValue={defaultValue} >
          <SelectTrigger className="w-[180px]">
            <SelectValue placeholder={placeholder || ''} />
          </SelectTrigger>
          <SelectContent >
            <SelectGroup>
              
              {
                options.map((i: Option) => {
                  return (
                  <SelectItem key={i.value} value={i.value}>{i.label}</SelectItem>
                )})
              }
            </SelectGroup>
          </SelectContent>
        </Select>
      </div>
      <p className="text-orange-500">
        {error?.message?.toString()}
      </p>
    </BiFieldContainer>
  )
}

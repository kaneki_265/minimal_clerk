import { LoadingSpinner } from "./loading";

export const PageLoading = () => {

  return (
    <div id="page-progress" className="fixed inset-0 flex items-center justify-center">
      <div className="absolute w-full h-full bg-white opacity-60">
         <LoadingSpinner 
         className="absolute top-1/2 left-1/2 z-2"
         />
      </div>
    </div>
  )
};

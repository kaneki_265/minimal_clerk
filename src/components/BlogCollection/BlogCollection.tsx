import { articleList } from '@/constants/articleList';
import React from 'react';
import CardItem from './CardItem';

const BlogCollection = () => {
  return (
    <div className="flex flex-wrap justify-center">
      {articleList.map((article,index) => (
        <CardItem
         key={index}
         article={article}
        />
      ))}
    </div>
  );
};

export default BlogCollection;

'use client'
import { ArticleItemType } from '@/constants/articleList';
import React, { useState } from 'react';
import { useRouter } from 'next/navigation'
import { PageLoading } from '../ui/page-loading';
import { useLoadArticle } from '@/hooks/useLoadArticle';

interface CardItemProps {
  article: ArticleItemType;
}

const CardItem: React.FC<CardItemProps> = ({ article }) => {
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  const { loadArticle } = useLoadArticle();

  const handleClick = async () => {
    try {
      setLoading(true);
      await loadArticle(article?.url);  
      setLoading(false);
      router.push('/');
    } catch(e) {
      console.error("Error loading article:", e);
    }
  };

  return (
    <div className="max-w-sm mx-2 my-4 bg-white rounded-lg overflow-hidden shadow-md cursor-pointer hover:bg-gray-100 w-80" onClick={handleClick}>
      {loading && <PageLoading />}
      <div className="px-6 py-4">
        <div className="font-bold text-xl mb-2">{article.title}</div>
        <p className="text-gray-700 text-base">{article.author}</p>
      </div>
    </div>
  );
};

export default CardItem;

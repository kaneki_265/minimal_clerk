import Image from "next/image"
import Link from "next/link"

const Logo = () => {
  return (
    <Link href="/" className="flex ml-4 items-center text-dark dark:text-light">
        {/* <div className=" w-10 md:w-12 overflow-hidden  mr-2 md:mr-4">
            <Image src='/logo.svg' alt="Bisounds Logo" 
              className="w-full h-auto" 
              width={5}
              height={5}
              priority 
            />
        </div> */}
        <span className="font-bold dark:font-semibold text-lg md:text-xl">BiSounds</span>
    </Link>
  )
}

export default Logo;
'use client';
import React from 'react';
import { Menu } from 'lucide-react';
import { useToggleStore } from '@/states/common';
import { useIsMobile } from '@/hooks/useIsMobile';
import MobileMenu from '../SideBar/MobileMenu';


const MenuIcon = () => {
  const changeToggle = useToggleStore((state: any) => state.changeToggle)

  const toggleSidebar = () => {
    changeToggle();
  }

  const isMobile = useIsMobile();

 if (!isMobile) {
   return (
     <div className="rounded border-2 border-black-500 p-2 cursor-pointer ">
       <Menu 
         onClick={toggleSidebar}
       />
     </div>
   )
 }

 return (
  <MobileMenu />
 )
}

export default MenuIcon
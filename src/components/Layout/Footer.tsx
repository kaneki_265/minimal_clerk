import { URL_BLOG, URL_CONTACT, URL_FEATURES } from '@/constants/url';
import Link from 'next/link';
import React from 'react';

const Footer = () => {
  return (
    <footer className="bg-black text-white py-8  bottom-0 w-full">
      <div className="container mx-auto flex flex-col md:flex-row max-w-6xl">
        {/* Company section */}
        <div className="lg:w-4/6 md:w-4/6 mb-4 md:mb-0">
          <Link href="/" className="items-center  gap-2 sm:flex">
            <p className="mb-2 text-xl font-bold transition-all  md:block dark:border-white">
              BiSounds
            </p>
          </Link>
          <p className="text-sm"> Listening to Any Blog Articles in Multiple Languages.</p>
        </div>
        {/* Menu section */}
        <div className="md:w-1/6 mb-4 md:mb-0 md:text-center">
          <h3 className="text-lg font-bold mb-2">Menu</h3>
          <ul className="text-sm">
            <li className="mb-1"><a href={URL_FEATURES} className="hover:underline">Features</a></li>
            <li className="mb-1"><a href={URL_BLOG} className="hover:underline">Blog</a></li>
          </ul>
        </div>
        {/* Help section */}
        <div className="md:w-1/6 md:text-center">
          <h3 className="text-lg font-bold mb-2">Help</h3>
          <ul className="text-sm">
            <li className="mb-1"><a href={URL_CONTACT} className="hover:underline">Contact</a></li>
          </ul>
        </div>
      </div>
    </footer>
  );
};

export default Footer;

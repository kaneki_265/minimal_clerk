import Link from "next/link";
import React from "react";
import { LogIn } from "lucide-react";
import { LogOut } from "lucide-react";
import { Button } from "@/components/ui/button";
import UserAvatar from "./UserAvatar";
import { UserButton, auth } from "@clerk/nextjs";



const UserAccountNav = () => {

  const { userId } = auth();
  console.log("UserAccountNav ~ userId:", userId)
  const isAuth = !!userId;

  return (
    <>
      {
        !isAuth && (
          <Link href="/sign-in">
            <Button>
              LOGIN
            </Button>
          </Link>
        )
      }
      {
        isAuth && (
          <UserButton afterSignOutUrl="/" />
        )
      }

    </>

  )
};

export default UserAccountNav;

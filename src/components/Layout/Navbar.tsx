import Link from "next/link";
import React from "react";
import { SignedIn, SignedOut, UserButton } from '@clerk/nextjs'
import Logo from "./Logo";
import MenuIcon from "./MenuIcon";
import UserAccountNav from "./UserAccountNav";

type Props = {};

const Navbar = async (props: Props) => {
  
  return (
    <nav className="fixed inset-x-0 top-0 bg-white dark:bg-gray-950 z-[10] h-fit border-b border-zinc-300 py-6 md:py-4">
      <div className="flex items-center justify-center h-full gap-2 px-4 mx-auto sm:justify-between 
      ">
        <div className="flex items-center">
          <MenuIcon />
          <Logo />
        </div>
        <div className="flex items-center gap-2 md:gap-8 ml-10">
          <UserAccountNav />

          {/* <Link href={URL_FEATURES} className="mr-3">
            Features
          </Link>
          <Link href={URL_CONTACT} className="mr-3">
            Contact
          </Link> */}
        </div>
      </div>
    </nav>
  );
};

export default Navbar;


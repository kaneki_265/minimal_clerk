import React from 'react'

interface Props {
  children: React.ReactNode
}

const MainContainer = ({
  children
}: Props) => {


  return (
    <main
      className={`
      mt-4
      flex-1
      `}
    >
      {children}
    </main>
  )
}

export default MainContainer


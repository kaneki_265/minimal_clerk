import React, { useState, useEffect, useRef } from "react";
import { Play, Pause } from 'lucide-react';

interface AudioPlayerProps {
  audioUrl: string;
  isVisible: boolean;
}

const AudioPlayer: React.FC<AudioPlayerProps> = ({ audioUrl, isVisible }) => {
  const [isPlaying, setIsPlaying] = useState(false);
  const [progress, setProgress] = useState(0);
  const audioRef = useRef<HTMLAudioElement>(null);

  useEffect(() => {
    if (isVisible && audioRef.current) {
      audioRef.current.play();
      setIsPlaying(true);
    }
  }, [isVisible]);

  useEffect(() => {
    const updateProgress = () => {
      if (audioRef.current) {
        const currentTime = audioRef.current.currentTime;
        const duration = audioRef.current.duration;
        const calculatedProgress = (currentTime / duration) * 100;
        setProgress(calculatedProgress);
      }
    };

    const intervalId = setInterval(updateProgress, 100);

    return () => {
      clearInterval(intervalId);
    };
  }, []);

  useEffect(() => {
    const audioElement = audioRef.current;
    const handleEnded = () => {
      setIsPlaying(false);
      audioElement!.currentTime = 0;
    };

    if (audioElement) {
      audioElement.addEventListener('ended', handleEnded);
      return () => {
        audioElement.removeEventListener('ended', handleEnded);
      };
    }
  }, []);

  const handlePlayPause = () => {
    if (audioRef.current) {
      if (isPlaying) {
        audioRef.current.pause();
      } else {
        audioRef.current.play();
      }
      setIsPlaying(!isPlaying);
    }
  };

  const handleProgressBarClick = (e: React.MouseEvent<HTMLDivElement>) => {
    if (audioRef.current) {
      const progressBar = e.currentTarget;
      const rect = progressBar.getBoundingClientRect();
      const offsetX = e.clientX - rect.left;
      const width = rect.width;
      const seekTime = (offsetX / width) * audioRef.current.duration;
      audioRef.current.currentTime = seekTime;
    }
  };

  return (
    <div className={`fixed bottom-0 left-0 w-full bg-gray-100 shadow-lg px-4 py-2 ${isVisible ? '' : 'hidden'}`}>
      <audio src={audioUrl} ref={audioRef}></audio>
      <div className="flex justify-between items-center">
        <button onClick={handlePlayPause} className="bg-gray-800 text-white px-4 py-4 rounded-full">
          {isPlaying ? <Pause style={{ borderRadius: '50%' }} /> : <Play style={{ borderRadius: '50%' }} />}
        </button>
        <div className="w-full mx-4 bg-gray-200 h-2 rounded-full overflow-hidden" onClick={handleProgressBarClick}>
          <div className="h-full bg-gray-800" style={{ width: `${progress}%`, borderRadius: 'inherit' }}></div>
        </div >
        <div className="w-44">{formatTime(audioRef.current ? audioRef.current.currentTime : 0)} / {formatTime(audioRef.current ? audioRef.current.duration : 0)}</div>
      </div>
    </div>
  );
};

const formatTime = (time: number): string => {
  const minutes = Math.floor(time / 60);
  const seconds = Math.floor(time % 60);
  return `${pad(minutes)}:${pad(seconds)}`;
};

const pad = (num: number): string => {
  return num < 10 ? `0${num}` : `${num}`;
};

export default AudioPlayer;

import React, { useRef } from 'react'
import { Play } from 'lucide-react';
import axios from 'axios';
import { convertBase64ToUrl } from '@/lib/tts/convertBase64ToUrl';
import { useToast } from '../ui/use-toast';
import { languagesArray } from '@/constants/languages';
import { ComponentLoading } from '../ui/component-loading';
import AudioPlayer from './AudioPlayer';

type Props = {
  label?: string,
  text?: string,
  lang?: string
  usedIndex: number,
  setUsedIndex: any,
  id: number
}

const PlayButton = (props: Props) => {
  const { label, text, lang , setUsedIndex, id, usedIndex} = props;
  const [audioUrl, setAudioUrl] = React.useState('');
  const [ loading, setLoading ] = React.useState(false)
  const { toast } = useToast();
  const [showAudioPlayer, setShowAudioPlayer] = React.useState(false);

  const onClick = async () => {
    if ( usedIndex === id ) {
      return
    }
    setLoading(true)
    try {
      const response = await axios.post("/api/speech", {
        lang: lang,
        text:  text
      });

      const base64Audio = response?.data;
      const url = convertBase64ToUrl(base64Audio);
      setUsedIndex(id)
      setAudioUrl(url);
      setShowAudioPlayer(true);

    } catch (e){
       console.log("convert to audio error", e);
       toast({
        title: "Error",
        description: "Can't play audio",
        variant: "destructive",
      });
    } finally { setLoading(false)}

  }
  return ( 
    <div className="flex flex-col justify-start relative">
 
      <div 
       id="khang"
        className={`flex gap-2 mb-3 
        cursor-${ text? 'pointer': 'not-allowed'}
         `}
        onClick={onClick}
       
      >
        {loading && <ComponentLoading />}
        <Play 
          color='#304BAB' 
         />
        <p className="text-blue-700">
          {!label ?  `Listen in ${languagesArray.find((i) =>i.value === lang)?.label}` : label}
        </p>
      </div>
      
      {(usedIndex === id ) && showAudioPlayer && (
        <AudioPlayer 
          // audioUrl="https://cdn.freesound.org/previews/728/728612_5674468-lq.mp3" 
          audioUrl={audioUrl} 
          isVisible={showAudioPlayer}
        />

      )}
    </div>
  )
}

export default PlayButton


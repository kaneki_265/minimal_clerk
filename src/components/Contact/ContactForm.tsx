"use client";
import React from "react";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod"
import { useForm } from "react-hook-form";
import BiInput from "../ui/custom/BiInput";
import { Button } from "../ui/button";
import axios from "axios";

export default function ContactForm() {
 
  const formSchema = z.object({
    email: z.string().email(),
    message: z.string().nonempty(),
    name: z.string().nonempty(),
  })

  const {
    handleSubmit,
    control,
    reset
  } = useForm({
    mode: "onTouched",
    resolver: zodResolver(formSchema),
    defaultValues: {
      name: "",
      email: "",
      message: ""
    }
  });

  const onSubmit = async ( data: any) => {
    try {
     await axios.post("/api/sendEmail", data);
    } catch (e) {
      console.error("submit translate form error", e)

    } finally {
      reset();
    }
  }

  return (
    <div className="flex justify-center">
      <form className="mb-4 flex-col w-full max-w-xl"  onSubmit={handleSubmit(onSubmit)}>
        <div
         className="mb-4 flex-col"
         >
          <BiInput
            name='name'
            label='Name'
            md={12}
            control={control}
          />
          <BiInput
            name='email'
            label='Email'
            md={12}
            control={control}
          />
          <BiInput
            name='message'
            label='Message'
            isTextArea
            rows={4}
            md={12}
            control={control}
          />
        </div>
        <div className="flex justify-center ">
          <Button>
            Submit
          </Button>
        </div>
      </form>
    </div>
  );
}

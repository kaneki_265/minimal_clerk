'use client';
import React, { useState } from 'react'
import BiInput from '../ui/custom/BiInput';
import { z } from "zod";
import { useForm } from 'react-hook-form';
import { zodResolver } from "@hookform/resolvers/zod"
import { PageLoading } from '../ui/page-loading';
import { Button } from '../ui/button';
import { useLoadArticle } from '@/hooks/useLoadArticle';

const LinkInput = () => {
  const [ loading , setLoading ] = useState(false);
  const formSchema = z.object({
    link: z.string().url(),
  })
  const { loadArticle } = useLoadArticle()

  const {
    handleSubmit,
    control,
  } = useForm({
    mode: "onTouched",
    resolver: zodResolver(formSchema),
    defaultValues: {
      link: "",
      langage: "en"
    }
  });

  const onSubmit = async ( data: any) => {
    setLoading(true);
    await loadArticle(data?.link);
    setLoading(false)
  };


  return (
    <div className="flex flex-col" >
      { loading && (<PageLoading />)}
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="flex mb-4 gap-4 flex-col sm:flex-row">
          <BiInput
            name='link'
            label='Add Article'
            md={8}
            control={control}
            placeholder='Example: https://blog.com/article'
          />
        </div >
        <div>
          <Button>
            Load Article
          </Button>
        </div>
      
      </form > 
    </div>
  )

}

export default LinkInput
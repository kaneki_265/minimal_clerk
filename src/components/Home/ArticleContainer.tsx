'use client'
import { useArticleStore } from '@/states/article'
import React from 'react'
import LoadArticleForm from './LoadArticleForm'
import TranslateContainer from './TranslateContainer'
import ArticleUtil from './ArticleUtil'

const ArticleContainer = () => {

  const isLoaded = useArticleStore((state) => state.isLoaded)

  return (
  <div className="flex my-4 flex-col h-full gap-2 px-8 mx-auto  max-w-6xl w-full">
    {
      !isLoaded && (
        <LoadArticleForm />
      )
    }
    {
      isLoaded && (
      // true && (
        <ArticleUtil />
      )
    }
  </div>
  )
}

export default ArticleContainer
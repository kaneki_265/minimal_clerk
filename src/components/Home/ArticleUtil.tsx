'use client'
import { TabItemValue } from '@/constants/menuItems';
import React, { useCallback, useState } from 'react'
import TabBar from '../MainTab/TabBar';
import TranslateForm from './TranslateForm';
import NewArticleButton from './NewArticleButton';

const ArticleUtil = () => {
  const [currentTab, setCurrentTab] = useState<TabItemValue>(TabItemValue.TranslateListen);

  const onChangeTab = (value: TabItemValue) => {
    setCurrentTab(value);
  };

  const renderTab = useCallback(() => {
    switch (currentTab) {
      case TabItemValue.TranslateListen:
        return <TranslateForm />;
      default:
        return <TranslateForm />;
    }
  }, [currentTab,]);


  return (
    <div className="flex flex-col h-full gap-2 px-8 mx-auto  max-w-6xl w-full">
      <NewArticleButton />
      <TabBar 
        activeValue={currentTab}
        onChange={onChangeTab}
      />
       {
        renderTab()
        }
    </div>
  )
}

export default ArticleUtil
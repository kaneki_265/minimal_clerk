'use client'
import React from 'react'
import { Button } from '../ui/button'
import { Plus } from 'lucide-react';
import { useArticleStore } from '@/states/article';

const NewArticleButton = () => {
  const setLoaded = useArticleStore((state) => state.setLoaded)
  const handleClick = () => {
    setLoaded(false);
  }

  return (
  <div className="flex justify-end">
    <Button variant="outline" onClick={handleClick}>
      <Plus className='mr-2'/>
       Load new Article
    </Button>
  </div>
  )
}

export default NewArticleButton
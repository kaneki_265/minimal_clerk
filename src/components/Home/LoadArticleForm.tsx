import React from 'react'
import LinkInput from './LinkInput'

const LoadArticleForm = () => {
  return (
    <div className="flex flex-col">
      <div className="flex justify-center mt-6 mb-10">
        <h1 className="text text-center  text-xl sm:text-4xl">
          Listening to Blog Articles and News in your Own Languages
        </h1>
        
    </div>
    <LinkInput />
    </div>
  )
}

export default LoadArticleForm
'use client';
import React, { useState } from 'react'
import { Button } from '../ui/button'
import { BiSelect } from '../ui/custom/BiSelect';
import PlayButton from '../Audio/PlayButton';
import { useForm } from 'react-hook-form';
import ArticleViewer from '../Article/ArticleViewer';
import { languagesArray } from '@/constants/languages';
import { z } from "zod"
import { zodResolver } from "@hookform/resolvers/zod"
import { PageLoading } from '../ui/page-loading';
import { useToast } from '../ui/use-toast';
import { translateText } from '@/lib/translate/translateText';
import { useArticleStore } from '@/states/article';

const TranslateForm = () => {
  // const [ sourceText, setSourceText] = React.useState('');
  const [ targetText, setTargetText ] = React.useState('');
  const [ targetLang, setTargetLang ] = useState('');
  const [ loading , setLoading ] = useState(false);
  const { toast } = useToast();

  const formSchema = z.object({
  })

  const {
    handleSubmit,
    control,
    watch
  } = useForm({
    mode: "onTouched",
    resolver: zodResolver(formSchema),
    defaultValues: {
      langage: "en"
    }
  });

  const selectedLang = watch("langage");
  const sourceText = useArticleStore((state: any) => state.sourceText)
  const sourceLang = useArticleStore((state: any) => state.sourceLang)
  const isMultipleLang = !!targetLang && targetLang !== sourceLang;
  const [usedIndex, setUsedIndex] = useState(0);

  const onSubmit = async () => {
    setLoading(true)
    try {
      const tartT = await translateText(sourceText, sourceLang, selectedLang)
      setTargetText(tartT);
      setTargetLang(selectedLang);
      
    } catch (e) {
      console.error("submit translate form error", e)
      toast({
        title: "Error",
        description: "Can't get article content",
        variant: "destructive",
      });

    } finally {
      setLoading(false)
    }
  };

  return (
    <div className="flex flex-col mb-12" >
      { loading && (<PageLoading />)}
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="flex mb-4 flex-col sm:flex-row">
          <BiSelect
            name='langage'
            label='Translate to'
            options={languagesArray}
            control={control}
            md={4}
            defaultValue='en'
          /> 
          <div className="mt-10">
            <Button>
              Translate and Listen
            </Button>
          </div>
        </div>
      </form>
      { sourceLang && !loading && (
      <div>

      <div 
        className="flex gap-12 my-4  flex-col   
        sm:flex-row sm:justify-start sm:items-start" 
      >
        <PlayButton
         id={1}
         lang={ sourceLang}
         text={sourceText}
         usedIndex={usedIndex}
         setUsedIndex={setUsedIndex}
        />
        {
          isMultipleLang && (
            <PlayButton
             id={2}
             text={targetText}
             lang={targetLang}
             usedIndex={usedIndex}
             setUsedIndex={setUsedIndex}
            />
          )
        }
      </div> 
      <ArticleViewer 
        isMultiple={isMultipleLang}
        sourceText={sourceText}
        targetText={targetText}
      />
      </div> 
      )}
    </div>
  )
}

export default TranslateForm
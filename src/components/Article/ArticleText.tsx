import { cn } from '@/lib/utils';
import React from 'react'

interface Props {
  isPrimary?: boolean,
  text: string
}

const ArticleText = (props: Props) => {
  const {
    isPrimary = false,
    text
  } = props;

  return (
    <p 
     className={cn({
      "text-blue-700": isPrimary,
      "text-black": !isPrimary,
    })}
    >
      { text}
    </p>
  )
}

export default ArticleText
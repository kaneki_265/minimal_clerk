'use client'
import React, { useState, useEffect } from 'react';
import { createSentencePairs } from '@/lib/utils/createSentencePairs';
import ArticleText from './ArticleText';

type Props = {
  sourceText: string,
  targetText: string,
  isMultiple: boolean,
}

const ArticleViewer = ({ 
  sourceText,
  targetText,
  isMultiple
  }: Props) => {

  const sentencePairs = createSentencePairs(sourceText, targetText);

  return (
    <div className="flex flex-col">
      {
        sentencePairs.map((i, index) => (
          <div key={index} className="flex flex-col mb-4">
            <ArticleText
             text={i.source}
            />
            {
              isMultiple && (
                <ArticleText
                 isPrimary
                 text={i.target}
                />
              )
            }
          </div>
        ))
      }
    </div>
  )
};

export default ArticleViewer;

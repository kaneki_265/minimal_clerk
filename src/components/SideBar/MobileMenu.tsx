import { sidebarMenu } from '@/constants/sidebarMenu';
import { Menubar, MenubarContent, MenubarItem, MenubarMenu, MenubarTrigger } from '@radix-ui/react-menubar'
import { Menu } from 'lucide-react';
import Link from 'next/link';
import React from 'react'

const MobileMenu = () => {
  return (

    <Menubar id='mobile-menu'>
    <MenubarMenu>
      <MenubarTrigger> 
        <div className="rounded border-2 border-black-500 p-2 cursor-pointer ">
          <Menu />
        </div>
      </MenubarTrigger>
      <MenubarContent className="bg-gray-800 rounded-md p-4">
        {
          sidebarMenu.map(i => (
            <Link key={i.key} href={i.route}>
              <MenubarItem 
              className="cursor-pointer flex py-1 hover:text-gray-300 text-lg text-white">
                {i.label}
              </MenubarItem>
            </Link>
          ))
        }
        {/* <MenubarSeparator /> */}
      </MenubarContent>
    </MenubarMenu>
  </Menubar>
   

  )
}

export default MobileMenu
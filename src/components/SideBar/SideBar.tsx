'use client';

import { sidebarMenu } from '@/constants/sidebarMenu';
import Link from 'next/link';
import ListItem from './ListItem';
import { useToggleStore } from '@/states/common';
import MobileMenu from './MobileMenu';
import { useIsMobile } from '@/hooks/useIsMobile';

const Sidebar = () => {

  const isOpen = useToggleStore((state: any) => state.isOpen)
  const isMobile = useIsMobile();
  if (isMobile) {
    return (<></>)
  }

  return (
    <div 
    id='sidebar-top'
    className={`
      inset-y-0 
      left-0 z-8 
      ${!isOpen ? 'w-16': 'w-72'}
      md:block
      h-full 
      overflow-y-auto`}
     >
      <div className={`
           fixed h-full  bg-gray-800
           ${!isOpen ? 'w-16': 'w-72' }
      `}
 
      >
        <div className="py-6 px-6">
          <ul className="text-white">
            {
              sidebarMenu.map(i => (
                <ListItem
                  key={i.key}
                  id={i.key}
                  isOpenDrawer={isOpen}
                  text={i.label}
                  icon={i.icon}
                  url={i.route}

                />
              )
            )
            }
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Sidebar;

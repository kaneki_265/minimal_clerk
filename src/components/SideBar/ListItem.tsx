

import React from 'react'
import Link from 'next/link';

interface ListItemProps {
  id?: string;
  text: string;
  isOpenDrawer?: boolean;
  icon: React.ReactNode;
  onClick?: () => void;
  url: string
}

const ListItem = ({
  id,
  isOpenDrawer,
  icon,
  text,
  onClick,
  url
}: ListItemProps) => {


  return (
    <li key={id} className="mb-4">
      <Link href={url}>
        <div className="flex py-1 hover:text-gray-300">
          {icon}
          {
            isOpenDrawer && (
              <p className=" pl-4">{text}</p>
            )
          }
        </div>
      </Link>
    </li>
  )
}

export default ListItem
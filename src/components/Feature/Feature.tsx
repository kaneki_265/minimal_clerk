
interface FeatureProps {
  icon: React.ReactNode;
  label: string;
  description: string;
}

export const Feature: React.FC<FeatureProps> = (
  { icon, label, description }
  ) => {
  return (
    <div className="lg:w-1/3 p-4">
      <div className="border rounded-lg p-6">
        <div className="text-center">{icon}</div>
        <div className="mt-4">
          <h3 className="text-xl font-semibold">{label}</h3>
          <p className="mt-2 text-gray-600">{description}</p>
        </div>
      </div>
    </div>
  );
};

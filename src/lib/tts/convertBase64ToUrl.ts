

export const convertBase64ToUrl = (base64Audio: any) => {
  const concatenatedBase64 = base64Audio.reduce((accumulator: any, currentItem: any) => {
    // Concatenate the base64 values
    return accumulator + currentItem.base64;
  }, '');

  // const byteCharacters = atob(base64Audio);
  const byteCharacters = atob(concatenatedBase64);
  const byteNumbers = new Array(byteCharacters.length);
  for (let i = 0; i < byteCharacters.length; i++) {
    byteNumbers[i] = byteCharacters.charCodeAt(i);
  }
  const byteArray = new Uint8Array(byteNumbers);
  const blob = new Blob([byteArray], { type: 'audio/wav' });

  // Convert Blob to Data URL
  const url = URL.createObjectURL(blob);

  return url;
}
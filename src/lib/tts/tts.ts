
import * as googleTTS from 'google-tts-api'; 

export const tts = async ( 
  text: string, 
  lang: string,
   )=> {

  const base64s = await googleTTS.getAllAudioBase64(text, {
    lang: lang,
    slow: false,
    host: 'https://translate.google.com',
    timeout: 10000,
  })

  return  base64s
}
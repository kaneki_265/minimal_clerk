import { extract } from '@extractus/article-extractor';
import { convert } from 'html-to-text';
import { addPeriodIfMissing, formatArticle } from './formatArticle';
import { txtT } from './sand';


export const articleExtract = async (url: string) => {
  try {
   const article = await extract(url);
  //  const article = {
  //   title: "",
  //   content: txtT
  //  }
  //  return article

    const title = addPeriodIfMissing(article?.title)
    const body = convert(article?.content || '', {
      wordwrap: null,
      formatters: {
        breakBlockFormatter: function (elem, walk, builder, formatOptions) {
          builder.openBlock({ leadingLineBreaks: 1 })
          builder.closeBlock({ trailingLineBreaks: 1 });
        }
      },
      selectors: [
        {
          selector: 'a',
          options: { ignoreHref: true }
        },
        {
          selector: 'img',
          format: 'skip'
        },
        {
          selector: 'svg',
          format: 'skip'
        },
        {
          selector: 'p',
          options: { leadingLineBreaks: 1, trailingLineBreaks: 1 }
        },
        {
          selector: 'div',
  
          options: { leadingLineBreaks: 1, trailingLineBreaks: 1 }
        },
       ]
      });

    const content = title + body;

    const formatText = formatArticle(content)


    return formatText;
  } catch (err) {
    console.error('Extract text error X:', err);
    return '';
  }

}
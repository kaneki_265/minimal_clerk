


export function addPeriodIfMissing(inputString: any): string {
  if (!inputString) {
    return ''
  }
  if (inputString.charAt(inputString.length - 1) !== '.') {
    return inputString + '. \n';
  }

  return inputString+ '\n';
}

export const formatArticle = ( originalText: string  ) => {
  // remove *
  const stringWithoutAsterisks = originalText.replace(/\*/g, '');

  // remove >
  const stringWithoutGreaterThan = stringWithoutAsterisks.replace(/>/g, '');

  // remove ---
  const stringWithoutDashes = stringWithoutGreaterThan.replace(/---/g, '');
  return stringWithoutDashes;
}
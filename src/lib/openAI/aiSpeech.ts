import OpenAI from "openai";

// const openai = new OpenAI();

const openai = new OpenAI({
  apiKey: "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", // This is the default and can be omitted
});

export async function aiSpeech() {
  const mp3 = await openai.audio.speech.create({
    model: "tts-1",
    voice: "alloy",
    input: "Today is a wonderful day to build something people love!",
  });

  const buffer = Buffer.from(await mp3.arrayBuffer());
  return buffer;
}


export async function aiSpeechWithRetry() {
  const maxRetries = 3;
  let currentRetry = 0;

  while (currentRetry < maxRetries) {
    try {
      const mp3 = await openai.audio.speech.create({
        model: "tts-1",
        voice: "alloy",
        input: "Today is a wonderful day to build something people love!",
      });

      const buffer = Buffer.from(await mp3.arrayBuffer());
      return buffer;
    } catch (error: any) {
      if (error.response && error.response.status === 429) {
        const waitTime = Math.pow(2, currentRetry) * 1000;
        await new Promise(resolve => setTimeout(resolve, waitTime));
        currentRetry++;
      } else {
        throw error;
      }
    }
  }

  throw new Error("Exceeded maximum number of retries");
}

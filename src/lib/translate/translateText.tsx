import translate from "translate";

// Function to break text into chunks
function chunkText(text: string, chunkSize: number) {
  const chunks = [];
  for (let i = 0; i < text.length; i += chunkSize) {
    chunks.push(text.slice(i, i + chunkSize));
  }
  return chunks;
}

async function translateChunk(
  chunk: string, 
  sourceLang: string,
  targetLang: string) {
  // translate.engine = "google";
  // fix issue with & 

  const andText =  await translate('and', {
    from: sourceLang,
    to: targetLang,
    engine: "google"  
  });

  let newText1 = chunk.replace(/&/g, andText ||'and' );
  let newText = newText1.replace(/#/g, "");


  return  translate(newText || '', {
      from: sourceLang,
      to: targetLang,
      engine: "google"  
  });
}


export const translateText = async (
  text: string, 
  sourceLang: string,
  targetLang: string, 
  ) => {


  try {
    const chunks = chunkText(text, 5000);
    const translations = await Promise.all(chunks.map(chunk => translateChunk(chunk, sourceLang, targetLang)));

    const translatedText = translations.join('');
    return translatedText;
  } catch (error) {
    console.error('Translate Eror', error);
    return ''
  }
}

const url = 'https://google-translate1.p.rapidapi.com/language/translate/v2';
const options = {
  method: 'POST',
  headers: {
    'content-type': 'application/x-www-form-urlencoded',
    'Accept-Encoding': 'application/gzip',
    'X-RapidAPI-Key': process.env.NEXT_PUBLIC_RAPID_GOOGLE_TRANSLATE || '',
    'X-RapidAPI-Host': 'google-translate1.p.rapidapi.com'
  }
};


const googleTranslate = async (
  text: string, 
  targetLang: string, 
  sourceLang: string
  ) => {

  const encodedParams = new URLSearchParams();
  encodedParams.set('q', text);
  encodedParams.set('target', targetLang);
  encodedParams.set('source', sourceLang);

  try {
    const response = await fetch(url, {
      ...options,
      body: encodedParams
    });

    const result = await response.text();
    console.log(result);
    return result
  } catch (error) {
    console.error(error);
  }
}





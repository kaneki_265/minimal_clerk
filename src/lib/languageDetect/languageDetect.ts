
// import * as LanguageDetectNpm  from 'languagedetect';
const LanguageDetectNpm = require('languagedetect');

export const languageDetect = ( text: string ) => {

  const lngDetector = new LanguageDetectNpm();
  lngDetector.setLanguageType('iso2')
  const lngArray = lngDetector.detect(text)
  const per =  lngArray?.[0]?.[1] || 0
  
  return  lngArray?.[0]?.[0] || 'en';
}


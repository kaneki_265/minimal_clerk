


export function createSentencePairs(source: string, target: string) {
  const sourceSentences = source?.split('\n');
  const targetSentences = target?.split('\n');


  return sourceSentences.map((src, index) => {
    return { source: src.trim(), target: targetSentences?.[index]?.trim() };
  });
}


import { languageDetect } from '@/lib/languageDetect/languageDetect';
import { useArticleStore } from '@/states/article';
import axios from 'axios';

export function useLoadArticle() {
  const articleStore = useArticleStore((state) => state)


  const loadArticle = async ( url: string) => {
    try {
      const article = await axios.post("/api/extract", {
        articleUrl: url
      });
      const arText = article?.data?.text || '';
      let last60Chars = arText.substr(-120);
      const detectedLng = languageDetect(last60Chars);

      articleStore.setSourceLang(detectedLng);
      articleStore.setSourceText(arText);
      articleStore.setLoaded(true)

    } catch (e) {
       console.error('load article error', e)
    } 
  }

  return {
    loadArticle
  }
}

import ArticleContainer from "@/components/Home/ArticleContainer";
import HomeContent from "@/components/Home/ArticleUtil";
import TranslateContainer from "@/components/Home/TranslateContainer";

export default function Home() {

 return (
  <div className="flex flex-col mx">
    {/* <HomeContent /> */}
    <ArticleContainer />
  </div>
 )
}


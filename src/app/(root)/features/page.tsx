
import { Feature } from "@/components/Feature/Feature";

export const metadata = {
  title: "Features",
  description: `Enter any blog URL to begin listening, Read blog articles aloud, Translate blogs into any language`,
};


export default function Features() {
  return (
    <div className="flex my-4 flex-col h-full gap-2 px-8 mx-auto  max-w-6xl w-full">
      <div className="flex justify-center mt-6 mb-10">
        <h1 className="text text-center  text-xl sm:text-4xl">
         The benefits of listening to any blog article through Bisounds.com
        </h1>
      </div>
      <div className="flex flex-wrap justify-center">
        <Feature
          icon={<IconEasyToUse />}
          label="Easy to Use"
          description="Enter any blog URL to begin listening"
        />
        <Feature
          icon={<IconTextToSpeech />}
          label="Text to Speech"
          description="Read blog articles aloud."
        />
        <Feature
          icon={<IconTranslation />}
          label="Translation"
          description="Translate blogs into any language for a deeper understanding and to support learning"
        />
      </div>
    </div>
  );
}


const IconEasyToUse: React.FC = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    className="h-12 w-12 mx-auto text-blue-500"
    fill="none"
    viewBox="0 0 24 24"
    stroke="currentColor"
  >
    <path
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={2}
      d="M9 10l5 5m0 0l5-5m-5 5V3"
    />
  </svg>
);

const IconTextToSpeech: React.FC = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    className="h-12 w-12 mx-auto text-blue-500"
    fill="none"
    viewBox="0 0 24 24"
    stroke="currentColor"
  >
    <path
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={2}
      d="M4 6h16a2 2 0 012 2v8a2 2 0 01-2 2H4a2 2 0 01-2-2V8a2 2 0 012-2zm8 10v-2m0 0H8m4 0h4m-4 0a4 4 0 100-8 4 4 0 000 8z"
    />
  </svg>
);

const IconTranslation: React.FC = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    className="h-12 w-12 mx-auto text-blue-500"
    fill="none"
    viewBox="0 0 24 24"
    stroke="currentColor"
  >
    <path
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={2}
      d="M19 14v5a2 2 0 01-2 2H7a2 2 0 01-2-2v-5m14-2V3a2 2 0 00-2-2H7a2 2 0 00-2 2v9m14-9h3a2 2 0 012 2v14a2 2 0 01-2 2h-3m-10-9h-4a2 2 0 00-2 2v14a2 2 0 002 2h4a2 2 0 002-2V7a2 2 0 00-2-2z"
    />
  </svg>
);
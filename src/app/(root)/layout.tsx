import MainContainer from "@/components/Layout/MainContainer"
import Navbar from "@/components/Layout/Navbar"
import Sidebar from "@/components/SideBar/SideBar"


const Layout = ({ children }: { children: React.ReactNode }) => {
  return (
    <main id="main-layout" className="root h-screen" >
       <Navbar />
      <div className="flex" >
        <Sidebar />
        <MainContainer> 
          {children}
        </MainContainer>
      </div> 
    </main>
  )
}

export default Layout

import ContactForm from "@/components/Contact/ContactForm";
import siteMetadata from "@/constants/siteMetadata";


export const metadata = {
  title: "Contact Us",
  description: `Contact me through the form available on this page or email me at ${siteMetadata.email}`,
};


export default function Contact() {
  return (
    <div className="flex my-4 flex-col h-full gap-2 px-8 mx-auto  max-w-6xl w-full">
      <div className="flex justify-center mt-6 mb-10">
        <h1 className="text text-center  text-xl sm:text-4xl">
         Contact Us
        </h1>
      </div>
      <ContactForm />
    </div>
  );
}

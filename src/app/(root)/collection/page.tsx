import BlogCollection from "@/components/BlogCollection/BlogCollection";


export const metadata = {
  title: "ArticleCollection",
  description: `A collection of excellent blog posts about startup, indie hacker, technical, working skill, and habits.`,
};


export default function ArticleCollection() {
  return (
    <BlogCollection />

  );
}

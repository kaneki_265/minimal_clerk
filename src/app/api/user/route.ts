import { NextResponse } from "next/server";
import { createUser } from "@/lib/actions/user.actions";

export async function POST(req: Request) {
  try {

    const user = {
      clerkId: '01',
      email: 'ab@c.com',
      username: 'mikasa',
      firstName: 'mikasa ',
      lastName: 'ackerman',
      photo: '',
    };

    const newUser = await createUser(user);
    return NextResponse.json(
      {
        user: newUser
      }, // data in FE response
      { status: 200 }
    );

  } catch (error) {
    console.error("google tts error", error);
    return NextResponse.json({ error: "google tts error" }, { status: 500 });
  }
}

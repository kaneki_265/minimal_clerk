import { NextResponse } from "next/server";
import { tts } from "@/lib/tts/tts";

export async function POST(req: Request) {
  try {

    const { lang,text} = await req.json();
    const result = await tts(text,lang)

    return NextResponse.json(
      result, // data in FE response
      { status: 200 }
    );

  } catch (error) {
    console.error("google tts error", error);
    return NextResponse.json({ error: "google tts error" }, { status: 500 });
  }
}

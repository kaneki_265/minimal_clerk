import { Env } from "@/constants/env";
import { NextResponse } from "next/server";

import nodemailer from "nodemailer";

export async function POST(req :Request) {
    
    const { name, email, message } = await req.json();

    const transporter = nodemailer.createTransport({
      host: 'smtp.gmail.com',
      port: 465,
      secure: true, // true for 465, false for other ports
      auth: {
        user: Env.email,
        pass: Env.gmailAppPassword, // gmail app not email
      }
    });


    // Define email options
    const mailOptions = {
      from: Env.email,
      to: Env.email,
      subject: "New Contact Form Submission",
      text: `Name: ${name}\nEmail: ${email}\nMessage: ${message}`,
    };

    try {
      // Send email
      await transporter.sendMail(mailOptions);
      return NextResponse.json(
        { message: "Email sent successfully" },
        { status: 200 }
      );
    } catch (error) {
      console.error("Error sending email:", error);
      return NextResponse.json(
        { message: "An error occurred while sending the email" },
        { status: 500 }
      );
    }
  
}

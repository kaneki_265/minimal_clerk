import { NextResponse } from "next/server";
import { articleExtract } from "@/lib/articleExtractor/articleExtract";

export async function POST(req: Request) {
  try {

    const { articleUrl} = await req.json();
    const result = await articleExtract(articleUrl)

    return NextResponse.json(
      {
        text: result,
      },
      { status: 200 }
    );

  } catch (error) {
    console.error("Extract article error", error);
    return NextResponse.json({ error: "Extract article error" }, { status: 500 });
  }
}

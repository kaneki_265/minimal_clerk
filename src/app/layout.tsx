import { cn } from "@/lib/utils";
import "./globals.css";
import { Lexend } from "next/font/google";
import { ClerkProvider } from "@clerk/nextjs";
import { GoogleAnalytics } from '@next/third-parties/google'
import siteMetadata from "@/constants/siteMetadata";
import { Env } from "@/constants/env";


const lexend = Lexend({ subsets: ["latin"] });

export const metadata = {
  metadataBase: new URL(siteMetadata.siteUrl!),
  title: {
    template: `%s | ${siteMetadata.title}`,
    default: siteMetadata.title, 
    // a default is required when creating a template
  },
  description: siteMetadata.description,
  openGraph: {
    title: siteMetadata.title,
    description: siteMetadata.description,
    url: siteMetadata.siteUrl,
    siteName: siteMetadata.title,
    images: [siteMetadata.socialBanner],
    locale: "en_US",
    type: "website",
  },
  robots: {
    index: true,
    follow: true,
    googleBot: {
      index: true,
      follow: true,
      noimageindex: true,
      "max-video-preview": -1,
      "max-image-preview": "large",
      "max-snippet": -1,
    },
  }
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <ClerkProvider appearance={{
      variables: { colorPrimary: '#624cf5' }
    }}>
      <html lang="en">
        <body className={
          cn(
          lexend.className, 
          "antialiased min-h-screen pt-16"
          )}>
          <GoogleAnalytics gaId={Env.trackingId} />
          {/* <div className="flex flex-col min-h-screen"> */}
          {children}
        </body>
      </html>
    </ClerkProvider>
  );
}

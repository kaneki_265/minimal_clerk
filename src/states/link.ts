import { create } from 'zustand'

export const useUrlStore = create((set) => ({
  url: '',
  setUrl: () => set((link: string) => ({ url: link })),
  removeUrl: () => set({ url: '' }),
}))
import { create } from 'zustand'


export interface ArticleState {
  isLoaded: boolean;
  sourceText: string;
  targetText: string;
  sourceLang: string;
  setSourceLang: (text: string) => void; // Function to set the source text
  setSourceText: (text: string) => void; // Function to set the source text
  setTargetText: (text: string) => void; // Function to set the target text
  removeSourceText: () => void; // Function to remove the source text
  removeTargetText: () => void; // Function to remove the target text
  setLoaded: (load: boolean) => void; // Function to set the loaded state

}


export const useArticleStore = create<ArticleState>((set) => ({
  isLoaded: false,
  setLoaded: (load: boolean) => set({ isLoaded: load }),

  sourceText: '',
  // sourceText: 'In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used as a placeholder before the final copy is available.',
  setSourceText: (text: string) => set(({ sourceText: text })),
  removeSourceText: () => set({ sourceText: '' }),
  targetText: '',
  setTargetText: (text: string) => set({ targetText: text }),
  removeTargetText: () => set({ targetText: '' }),
  sourceLang: 'en',
  setSourceLang: (text: string) => set({ sourceLang: text }),
}))


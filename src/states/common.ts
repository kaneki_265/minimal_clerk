import { create } from 'zustand'

interface ToggleState {
  isOpen: boolean;
}

export const useToggleStore = create((set) => ({
  isOpen: true,
  changeToggle: () => set((state: ToggleState) => ({ isOpen: !state.isOpen })),

}))